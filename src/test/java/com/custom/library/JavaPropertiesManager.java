package com.custom.library;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/***
 * 
 * @author Mierzhate Ailati
 * This class is used to read data from property file.
 *
 */
public class JavaPropertiesManager {
	private String propertiesFile;
	private Properties prop;
	private InputStream input;

	public JavaPropertiesManager(String propertiesFilePath) {
		propertiesFile = propertiesFilePath;
		prop = new Properties();
	}

	public String readProperty(String key) {
		String value = null;
		try {
			input = new FileInputStream(propertiesFile);
			prop.load(input);
			value = prop.getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}

}
