package com.custom.library;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import com.page.PageObject;

/***
 * 
 * @author Mierzhate Ailati This is BasePage Class. It is used for invoke the
 *         browser to the specific and close the browser for each test cases.
 *
 */

public class BasePage {
	public static WebDriver driver;
	public static GlobalSeleniumLibrary myLib;
	private static JavaPropertiesManager configProperty;
	private static String browser;

	public static PageObject po;

	/***
	 * Invoking the browser and navigate to the specific page, browser information
	 * and website information contain in the property
	 * file(src/test/resources.config.proeprties)
	 * 
	 * @throws Exception
	 */
	@BeforeMethod
	public void beforeEachTestStart() throws Exception {
		driver = myLib.startLocalBrowser(browser);
		driver.get(configProperty.readProperty("homePageURL"));
		po = new PageObject(driver);
	}

	/***
	 * Capturing the failed test screenshots
	 * 
	 * @param result
	 */
	@AfterMethod
	public void afterEachTestComplete(ITestResult result) {
		try {
			if (ITestResult.FAILURE == result.getStatus()) {
				myLib.captureScreenshot(result.getName(), "target/images/");
			}
			Thread.sleep(5 * 1000); // stops the test for 5 seconds
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.close(); // close the browser
		}
	}

	/***
	 * Terminate the driver process.
	 */
	@AfterClass
	public void afterAllTestsComplete() {
		if (driver != null) // if there is any open browser left, close it.
			driver.quit();

	}

	/***
	 * read the data from property file, which includes browser type.
	 */
	@BeforeClass
	public void beforeAllTestStart() {

		configProperty = new JavaPropertiesManager("src/test/resources/config.properties");
		browser = configProperty.readProperty("browserType");
		myLib = new GlobalSeleniumLibrary(driver);
	}

}
