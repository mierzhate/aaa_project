package com.custom.library;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;
import com.google.common.io.Files;

/***
 * 
 * @author Mierzhate Ailati This class contain multiple methods used for the
 *         test such as invoke the Chrome browser, Entering text to the text
 *         field , Handling check box, Capturing Screenshot.
 * 
 *
 */
public class GlobalSeleniumLibrary {

	private WebDriver driver;

	/***
	 * 
	 * @param _driver
	 */
	public GlobalSeleniumLibrary(WebDriver _driver) {
		driver = _driver;
	}

	/***
	 * If the test running on the Windows machine, please add ".exe" to the browser property 
	 * @return
	 */
	public WebDriver startChromeBrowser() {
		try {
			System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
			driver = new ChromeDriver(); // open Chrome browser
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		} catch (Exception e) {
			assertTrue(false);
		}
		return driver;
	}

	/***
	 * 
	 * @param inSeconds
	 */
	public void customWait(double inSeconds) {
		try {
			Thread.sleep((long) (inSeconds * 1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * 
	 * @param element
	 * @param userInputValue
	 */
	public void enterTextField(WebElement element, String userInputValue) {
		element.clear(); // clear the text box and delete the default value
		element.sendKeys(userInputValue);
		System.out.println("entering '" + userInputValue + "' to text-field");
	}

	/***
	 * 
	 * @param element
	 * @param isCheck
	 */
	public void handleCheckBox(WebElement element, boolean isCheck) {
		boolean checkBoxState = element.isSelected();
		if (isCheck == true) // user wanted to select the check-box
		{
			if (checkBoxState == true) // if box is checked by default
			{
				// do nothing
			} else // box is Not checked by default
			{
				element.click();
			}
		} else { // user Do not want to select the check- box
			if (checkBoxState == true) {
				element.click();
			} else {
				// Do nothing
			}
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * 
	 * @param screenshotFileName
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public String captureScreenshot(String screenshotFileName, String filePath) throws IOException {
		String screenCaptureFile = null;
		String tempTime = getCurrentTime();
		File tempFile = new File(filePath);
		if (!tempFile.isDirectory()) {
			tempFile.mkdir(); // create the folder
		}
		if (!filePath.isEmpty()) {
			screenCaptureFile = filePath + screenshotFileName + tempTime + ".png";
		} else {
			screenCaptureFile = "src/test/resources/screenshots/" + screenshotFileName + tempTime + ".png";
		}
		File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Files.copy(srcFile, new File(screenCaptureFile));
		File absFile = new File(screenCaptureFile);
		String absPath = absFile.getAbsolutePath();
		System.out.println("Screenshot Path: " + absPath);
		return screenCaptureFile;
	}

	/***
	 * This method gets the current time
	 * 
	 * @return String of current time-stamp with underscores
	 */
	public String getCurrentTime() {
		Date date = new Date();
		String tempTime = new Timestamp(date.getTime()).toString();
		String finalTimeStamp = tempTime.replace(":", "_").replace("-", "_").replace(" ", "_").replace(".", "_");
		// System.out.println("TempTime: " + tempTime);
		// System.out.println("FinalTime: " + finalTimeStamp);
		return finalTimeStamp;
	}

	/***
	 * 
	 * @param browser
	 * @return
	 */
	public WebDriver startLocalBrowser(String browser) {

		switch (browser) {
		// case "IE":
		// driver = startIEBrowser();
		// System.out.println("Starting 'IE' browser !");
		// break;
		case "Chrome":
			driver = startChromeBrowser();
			System.out.println("Starting 'Chrome' browser !");
			break;
		// case "Firefox":
		// driver = startFirefoxBrowser();
		// System.out.println("Starting 'Firefox' browser !");
		// break;
		default:
			driver = startChromeBrowser();
			System.out.println("User selected browser: " + browser + ", Starting default browser - Chrome!");
		}
		return driver;
	}

	/***
	 * 
	 * @param by
	 * @return
	 */
	public WebElement fluentWait(By by) {
		WebElement targetElem = null;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(3, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		targetElem = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(by);
			}
		});
		return targetElem;
	}

	/***
	 * 
	 * @param by
	 * @return
	 */
	public WebElement waitUntilPageLoadComplete(By by) {
		WebElement element = fluentWait(by);
		return element;
	}
}
