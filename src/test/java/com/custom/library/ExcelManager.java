package com.custom.library;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import jxl.Sheet;
import jxl.Workbook;

/***
 * 
 * @author Mierzhate Ailati This class is using apache.poi.xssf to reading data
 *         from excel files.
 *
 */
public class ExcelManager {

	private static XSSFSheet ExcelWSheet;
	private static XSSFCell Cell;
	private static String excelFile;

	public ExcelManager(String excelFilePath) {
		excelFile = excelFilePath;
	}

	public String[][] getExcelData(String sheetName) {
		String[][] arrayExcelData = null;
		try {
			FileInputStream fs = new FileInputStream(excelFile);
			Workbook wb = Workbook.getWorkbook(fs);
			Sheet sh = wb.getSheet(sheetName);

			int totalNoOfCols = sh.getColumns();
			int totalNoOfRows = sh.getRows();

			arrayExcelData = new String[totalNoOfRows - 1][totalNoOfCols];

			for (int i = 1; i < totalNoOfRows; i++) {
				for (int j = 0; j < totalNoOfCols; j++) {
					arrayExcelData[i - 1][j] = sh.getCell(j, i).getContents();
				}
			}

		} catch (Exception e) {

		}
		return arrayExcelData;
	}

	public static String getCellData(int RowNum, int ColNum) {
		String cellData = null;
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			cellData = Cell.getStringCellValue();
		} catch (Exception e) {

		}
		return cellData;
	}

}
