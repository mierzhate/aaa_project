package com.page;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.custom.library.GlobalSeleniumLibrary;

/***
 * 
 * @author Mierzhate Ailati This whole test using Page Object Model, in this
 *         class contain all the elements, assertion and actions used for
 *         customer registration
 *
 */
public class PageObject {

	WebDriver driver;
	GlobalSeleniumLibrary myLib;

	public PageObject(WebDriver driver) {
		this.driver = driver;
		myLib = new GlobalSeleniumLibrary(driver);
		PageFactory.initElements(driver, this);
	}

	// ===================HomePage WebElement========//
	@FindBy(name = "zip-code")
	private WebElement zipcodeBox;
	@FindBy(css = "#zipModal > div > div > a")
	private WebElement goBtn;

	@FindBy(linkText = "JOIN AAA")
	private WebElement joinBtn;

	// ===================Level Page WebElement========//
	@FindBy(id = "classicSelectButtonTop")
	private WebElement selectMembershipBtn;

	// ===================Registration Page WebElement========//
	@FindBy(id = "firstName")
	private WebElement firstName;

	@FindBy(id = "lastName")
	private WebElement lastName;

	@FindBy(xpath = ".//*[@name='primaryMemberGender'][@id='optionsRadios1']")
	private WebElement maleCheckbox;
	@FindBy(xpath = ".//*[@name='primaryMemberGender'][@id='optionsRadios2']")
	private WebElement femaleCheckbox;

	@FindBy(xpath = "//*[@tabindex='8']")
	private WebElement firstSubmitBtn;

	@FindBy(id = "exampleInputAddress")
	private WebElement addressTextField;

	@FindBy(id = "cityList")
	private WebElement cityTextField;

	@FindBy(id = "state")
	private WebElement stateTextField;

	@FindBy(id = "ZIP")
	private WebElement zipcodeTextField;

	@FindBy(id = "memberEmail")
	private WebElement emailTextField;

	@FindBy(xpath = ".//*[@name='contactInfoIsReceiveEmails'][@id='optionsRadios1']")
	private WebElement interestedCheckbox;
	@FindBy(xpath = ".//*[@name='contactInfoIsReceiveEmails'][@id='optionsRadios2']")
	private WebElement nonInterestedCheckbox;

	@FindBy(id = "exampleInputPhonenum")
	private WebElement phoneNumTextField;

	@FindBy(xpath = ".//*[@name='contactInfoIsCellPhone'][@id='optionsRadios1']")
	private WebElement cellphoneCheckbox;
	@FindBy(xpath = ".//*[@name='contactInfoIsCellPhone'][@id='optionsRadios2']")
	private WebElement nonCellphoneCheckbox;

	@FindBy(xpath = ".//*[@name='contactInfoIsAgreement'][@id='optionsRadios1']")
	private WebElement agreeConsentCheckbox;
	@FindBy(xpath = ".//*[@name='contactInfoIsAgreement'][@id='optionsRadios2']")
	private WebElement disagreeConsentCheckbox;

	@FindBy(xpath = "//*[@tabindex='18']")
	private WebElement contactSaveBtn;

	@FindBy(id = "noThanksToAddAssociateButton")
	private WebElement noAddMemberBtn;

	@FindBy(id = "noToAddOptionsLink")
	private WebElement noAddAssitanceBtn;

	@FindBy(id = "proceedToCheckoutButton")
	private WebElement checkOutBtn;

	// ==============Payment Page WebElement=========//
	@FindBy(id = "PaymentMethod")
	private WebElement paymentMethodDropdown;

	@FindBy(css = "#PaymentMethodDropDown > ul > li:nth-child(1) > a:nth-child(1)")
	private WebElement selectNewCreditCard;

	@FindBy(xpath = ".//*[@class='close']")
	private WebElement closePaymentMethodWindowBtn;

	@FindBy(id = "paymentSubmitBtn")
	private WebElement submitPaymentBtn;

	@FindBy(xpath = "//*[@id=\"valError_PaymentMethod\"]/div[@class='errorContent']")
	private WebElement paymentMethodWarningBox;

	// =============Assertion===========//

	public final String WarningMessageofPaymentMethod = "Payment Method is required.";

	// ===============Actions==============//

	public void enterZipCode(String zipcode) {
		myLib.enterTextField(zipcodeBox, zipcode);
		goBtn.click();
	}

	public void joinAAA() {
		joinBtn.click();
	}

	public void selectMembershipLevel() {
		selectMembershipBtn.click();
	}

	public void enterFirstName(String firstname) {
		myLib.enterTextField(firstName, firstname);
	}

	public void enterLastName(String lastname) {
		myLib.enterTextField(lastName, lastname);
	}

	public void selectGender(String gender) {
		if (gender.equalsIgnoreCase("Male")) {
			myLib.handleCheckBox(maleCheckbox, true);
			myLib.customWait(2);
		} else if (gender.equalsIgnoreCase("Female")) {
			myLib.handleCheckBox(femaleCheckbox, true);
			myLib.customWait(2);

		}

	}

	public void clickFirstNextButton() {
		firstSubmitBtn.click();
	}

	public void enterAddress(String address) {
		myLib.enterTextField(addressTextField, address);
	}

	public void enterCity(String city) {
		myLib.enterTextField(cityTextField, city);
	}

	public void enterState(String state) {
		myLib.enterTextField(stateTextField, state);
	}

	public void enterZip(String zipcode) {
		myLib.enterTextField(zipcodeTextField, zipcode);
	}

	public void enterEmailAdd(String email) {
		myLib.enterTextField(emailTextField, email);
	}

	public void selectCommunicationPreferences(String yesOrNo) {
		if (yesOrNo.equalsIgnoreCase("Yes")) {
			myLib.handleCheckBox(interestedCheckbox, true);
		} else if (yesOrNo.equalsIgnoreCase("No")) {
			myLib.handleCheckBox(nonInterestedCheckbox, true);

		}

	}

	public void enterPhoneNum(String phoneNum) {
		myLib.enterTextField(phoneNumTextField, phoneNum);
	}

	public void isThisACellPhone(String yesOrNo) {
		if (yesOrNo.equalsIgnoreCase("Yes")) {
			myLib.handleCheckBox(cellphoneCheckbox, true);
		} else if (yesOrNo.equalsIgnoreCase("No")) {
			myLib.handleCheckBox(nonCellphoneCheckbox, true);

		}

	}

	public void clickNextBtn() {
		contactSaveBtn.click();
		myLib.customWait(3);
	}

	public void noAddMember() {
		noAddMemberBtn.click();
		myLib.customWait(3);
	}

	public void cancelAddRoadAssistance() {
		noAddAssitanceBtn.click();
		myLib.customWait(3);
	}

	public void clickCheckOutBtn() {
		checkOutBtn.click();
	}

	// wait page load
	public void waitUntilPaymentPageLoadComplete() {
		WebElement elem = myLib.waitUntilPageLoadComplete(By.id("paymentForm"));
		Assert.assertNotNull(elem);
	}

	// select payment method and close
	public void clickPaymentMethodDropdown() {
		paymentMethodDropdown.click();
		myLib.customWait(2);
	}

	public void selectNewCreditCard() {
		selectNewCreditCard.click();
		myLib.customWait(2);
	}

	public void closePaymentMethodWindow() {
		closePaymentMethodWindowBtn.click();
		myLib.customWait(2);
	}

	public void clickSubmitPaymentBtn() {
		submitPaymentBtn.click();
		myLib.customWait(2);
	}

	public void verifyErrorMessageDisplay() {
		assertEquals(WarningMessageofPaymentMethod, paymentMethodWarningBox.getText());
	}

}
