package com.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.custom.library.BasePage;
import com.custom.library.ExcelManager;

/***
 * 
 * @author Mierzhate Ailati
 * 
 *         This is the test case for customer registration which contains
 *         DataProvider and Test case
 *         
 *         The whole test using page object model pattern, combined with data driven framework.
 * 
 *         There are two combinations of data included in the excel file, one
 *         for male registration, one for female registration.
 *
 */

public class RegistrationTest extends BasePage {
	/***
	 * Using DataProvider as Data Driven Framework  DataProvider read the customer
	 * data from excel files
	 */
	@DataProvider(name = "RegistrationInfo")
	public static Object[][] credentials() {
		ExcelManager reader = new ExcelManager("src/test/resources/testData.xls");
		return reader.getExcelData("Sheet2");
	}

	/***
	 * This is the test case for registration on the https://www.calif.aaa.com/ It
	 * contains each step for basic flow of member joining AAA.
	 * 
	 * @param FirstName
	 * @param LastName
	 * @param Gender
	 * @param Address
	 * @param City
	 * @param State
	 * @param Zipcode
	 * @param Email
	 * @param PhoneNum
	 * @param isACellPhoneOrNot
	 */
	@Test(dataProvider = "RegistrationInfo")
	public void registration(String FirstName, String LastName, String Gender, String Address, String City,
			String State, String Zipcode, String Email, String PhoneNum, String isACellPhoneOrNot) {
		po.enterZipCode("90007");
		po.joinAAA();
		po.selectMembershipLevel();
		po.enterFirstName(FirstName);
		po.enterLastName(LastName);
		po.selectGender(Gender);
		po.clickFirstNextButton();
		po.enterAddress(Address);
		po.enterCity(City);
		po.enterState(State);
		po.enterZip(Zipcode);
		po.enterEmailAdd(Email);
		po.selectCommunicationPreferences("No");
		po.enterPhoneNum(PhoneNum);
		po.isThisACellPhone(isACellPhoneOrNot);
		po.clickNextBtn();
		po.noAddMember();
		po.cancelAddRoadAssistance();
		po.clickCheckOutBtn();
		po.waitUntilPaymentPageLoadComplete();
		po.clickPaymentMethodDropdown();
		po.selectNewCreditCard();
		po.closePaymentMethodWindow();
		po.clickSubmitPaymentBtn();
		po.verifyErrorMessageDisplay();
	}
}
